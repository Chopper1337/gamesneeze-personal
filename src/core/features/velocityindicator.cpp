#include "features.hpp"
#include "../../includes.hpp"

void Features::VelocityIndicator::onSdl() {
    if (CONFIGBOOL("Misc>Misc>Movement>Velocity Indicator") && Globals::localPlayer && Globals::localPlayer->health() > 0) {
        // "Hacky way to do black shadow but it works" -sekc
        Globals::drawList->AddText(ImVec2((Globals::screenSizeX/2)-33, Globals::screenSizeY * 0.945), ImColor(0, 0, 0, 255), std::to_string(std::round(Globals::localPlayer->velocity().Length2D())).c_str());
        Globals::drawList->AddText(ImVec2((Globals::screenSizeX/2)-32, (Globals::screenSizeY * 0.945)-1), ImColor(255,255,255, 255), std::to_string(std::round(Globals::localPlayer->velocity().Length2D())).c_str());
    }
}